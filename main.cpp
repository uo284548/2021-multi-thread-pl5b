/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <CImg.h>

using namespace cimg_library;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char* SOURCE_IMG      = "hojas_2.bmp";
const char* BACKGROUND_IMG  = "background_bw_l_7.bmp";
const char* DESTINATION_IMG = "imagen_procesado.bmp";
struct parametros
{
	unsigned int pixelInicial;
	unsigned int pixelFinal;
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components of source image
	data_t *pRdest, *pGdest, *pBdest; // Pointers to the R, G and B components of destiny image
	data_t *pRbck, *pGbck, *pBbck; // Pointers to the R, G and B components of background image

};



void* Filtro(void* arg){

	const int repetitions = 45;
	parametros *n = (parametros*)arg;


	for(uint times= 0 ; times < repetitions ; times++){  // Loop that marks the times the algorithm is repeated
		for (uint i =n->pixelInicial; i < n->pixelFinal; i++){  // Loop that loops through all pixels in the image
			int r = 255 - ( ( 256 * ( 255 - *(n->pRbck + i) ) ) / ( *(n->pRsrc + i) + 1 ) );
			if (r < 0) {  // Checks if the red value of the pixel is bellow the allowed range
                *(n->pRdest + i) = 0;
            } else if (r > 255) {  // Checks if the red value of the pixel is over the allowed range
                *(n->pRdest + i) = 255;
            } else {
                *(n->pRdest + i) = r;
            }

			int g = 255 - ( ( 256 * ( 255 - *(n->pGbck + i) ) ) / ( *(n->pGsrc + i) + 1 ) );
            if (g < 0) {  // Checks if the green value of the pixel is bellow the allowed range
                *(n->pGdest + i) = 0;
            } else if (g > 255) {  // Checks if the green value of the pixel is over the allowed range
                *(n->pGdest + i) = 255;
            } else {
                *(n->pGdest + i) = g;
            }

			int b = 255 - ( ( 256 * ( 255 - *(n->pBbck + i) ) ) / ( *(n->pBsrc + i) + 1 ) );
            if (b < 0) {  // Checks if the blue value of the pixel is bellow the allowed range
                *(n->pBdest + i) = 0;
            } else if (b > 255) {  // Checks if the blue value of the pixel is over the allowed range
                *(n->pBdest + i) = 255;
            } else {
                *(n->pBdest + i) = b;
            }
		}
	}


	return NULL;
	
}



int main() {
	// Open file and object initialization
	const int N = 16;
	pthread_t thread[N];
    int i;
	parametros params[N];

	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> bckImage(BACKGROUND_IMG);
	uint width, height; // Width and height of the image
	uint nComp; // Number of image components
	data_t *pDstImage; // Poi i++; m(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel

	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum();

	// Display source image
	srcImage.display();
	
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components of source image
	data_t *pRdest, *pGdest, *pBdest; // Pointers to the R, G and B components of destiny image
	data_t *pRbck, *pGbck, *pBbck; // Pointers to the R, G and B components of background image

	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the componet arrays of the background image
	pRbck = bckImage.data(); // pRcomp points to the R component array
	pGbck = pRbck + height * width; // pGcomp points to the G component array
	pBbck = pGbck + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

	struct timespec tStart; // Object that will be used to save the inicial time of the algorithm
	struct timespec tEnd;   // Object that will be used to save the final time of the algorithm
	double dElapsedTimeS;   // The time the algorithm takes to finish (calculated by subtracting to the time in tEnd the time in tStart)

	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	if (clock_gettime(CLOCK_REALTIME, &tStart) != 0)
	{
		perror("gettime");
		exit(EXIT_FAILURE);
	}

	// Display background image
	bckImage.display();

	/*
	* Se inicaliza el primer termino de la estructura de datos params
	*/
	params[0].pixelInicial=0;
	int paso = width*height/N;
	params[0].pixelFinal=params[0].pixelInicial+paso;
	params[0].pRsrc=pRsrc;
	params[0].pBsrc=pBsrc;
	params[0].pGsrc=pGsrc;
	params[0].pRbck=pRbck;
	params[0].pBbck=pBbck;
	params[0].pGbck=pGbck;
	params[0].pRdest=pRdest;
	params[0].pBdest=pBdest;
	params[0].pGdest=pGdest;


	/*
	* Se inicaliza el resto de tÃ©rminos de la estructura de datos params
	*/
	for(int i= 1;i< N; i++){
		params[i].pixelInicial=params[i-1].pixelFinal;
		params[i].pixelFinal=params[i-1].pixelFinal+paso;
		params[i].pRsrc=pRsrc;
		params[i].pBsrc=pBsrc;
		params[i].pGsrc=pGsrc;
		params[i].pRbck=pRbck;
		params[i].pBbck=pBbck;
		params[i].pGbck=pGbck;
		params[i].pRdest=pRdest;
		params[i].pBdest=pBdest;
		params[i].pGdest=pGdest;
	}


	// Thread creation
    for (i = 0; i < N; i++)
    {
        if (pthread_create(&thread[i], NULL, Filtro, (void*)&params[i]) != 0)
        {
            fprintf(stderr, "ERROR: Creating thread %d\n", i);
            return EXIT_FAILURE;
        }
		
    }
	//Thread join
	for (i = 0; i < N; i++)
    {
        pthread_join(thread[i],(void**)pDstImage);
    }


	
	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */		
	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	if (clock_gettime(CLOCK_REALTIME, &tEnd) != 0)
	{
		perror("gettime");
		exit(EXIT_FAILURE);
	}


	dElapsedTimeS = tEnd.tv_sec - tStart.tv_sec;
	dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time: %f\n", dElapsedTimeS);
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);


	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);
	return 0;
}
